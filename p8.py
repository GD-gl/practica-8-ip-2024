import typing

# 1.1
def contar_lineas(nombre_archivo: str) -> int:
    lineas = 0
    with open(nombre_archivo, 'r') as f:
        lineas = len(f.readlines())
    return lineas

print(contar_lineas("test.txt"))

# 1.2
def existe_palabra(palabra: str, nombre_archivo: str) -> int:
    hay_palabra: bool = False
    with open(nombre_archivo, 'r') as f:
        for linea in f.readlines():
            if linea.count(palabra) > 0:
                hay_palabra = True
                break
    return hay_palabra

print(existe_palabra("haskell", "test.txt"))
print(existe_palabra("pucha", "test.txt"))

# 1.3
def cantidad_apariciones(nombre_archivo: str, palabra: str) -> int:
    apariciones: int = 0
    with open(nombre_archivo, 'r') as f:
        for linea in f.readlines():
            apariciones += linea.count(palabra)
    return apariciones

print(cantidad_apariciones("test.txt", "haskell"))

# 2
def clonar_sin_comentarios(nombre_archivo: str):
    contenido_nuevo = ""
    with open(nombre_archivo, 'r') as f:
        for linea in f.readlines():
            if not linea.replace(" ", "").startswith("#"):
                contenido_nuevo += linea
    with open("clonado_" + nombre_archivo, 'w') as f:
        f.write(contenido_nuevo)

clonar_sin_comentarios("test.txt")

# 3
def invertir_lineas(nombre_archivo: str):
    lineas: list[str] = []
    with open(nombre_archivo, 'r') as f:
        lineas = f.readlines()
    lineas.reverse()
    with open("reverso.txt", "w") as f:
        for v in lineas:
            f.write(v)

invertir_lineas("test.txt")

# 4
def agregar_frase_al_final(nombre_archivo: str, frase: str):
    open(nombre_archivo, "a").close() # lo creo
    with open(nombre_archivo, "r+") as f:
        lineas = f.readlines()
        lineas.append(frase)
        f.truncate(0)
        f.seek(0)
        f.writelines(lineas)

agregar_frase_al_final("frases.txt", "Holaaaa\n")

# 5
def agregar_frase_al_principio(nombre_archivo: str, frase: str):
    open(nombre_archivo, "a").close() # lo creo
    with open(nombre_archivo, "r+") as f:
        lineas = [frase]
        lineas.extend(f.readlines())
        f.truncate(0)
        f.seek(0)
        f.writelines(lineas)

agregar_frase_al_principio("frases.txt", "Holaaaa\n")

# 6
def es_caracter_de_palabra(char: str) -> bool:
    if not ((char >= 'a' and char <= 'z') or (char >= 'A' and char <= 'Z') or (char >= '0' and char <= '9') or char == ' ' or char == '_'):
        return False
    return True

def es_palabra(palabra: str) -> bool:
    if len(palabra) < 5:
        return False
    for char in palabra:
        if not es_caracter_de_palabra(char):
            return False
    return True

def listar_palabras_de_archivo(nombre_archivo: str) -> list[str]:
    palabras: list[str] = []
    with open(nombre_archivo, "rb") as f:
        palabra: str = ""
        while True:
            byte_lista: bytes = f.read(1)
            if len(byte_lista) < 1:
                break
            byte_char: str = chr(byte_lista[0])
            if es_caracter_de_palabra(byte_char):
                palabra += byte_char
                palabras.append(palabra)
            else:
                if len(palabra) > 4:
                    palabras.append(palabra)
                    palabra = ""
    return palabras

print(listar_palabras_de_archivo("test.txt"))
# print(listar_palabras_de_archivo("/usr/bin/bash"))

# 7
# no voy a modularizarlo jaja (bueno si, el promedio)
def promedio_lista_floats(v: list[float]) -> float:
    sumatoria = 0.0
    for i in v:
        sumatoria += i
    return sumatoria / float(len(v))

def calcular_promedio_por_estudiante(nombre_archivo_notas: str, nombre_archivo_promedios: str):
    diccionario_notas: dict[str, list[float]] = {}
    with open(nombre_archivo_notas, 'r') as f:
        for linea in f.readlines():
            [lu, _, _, nota] = linea.split(',')
            notas_lu = []
            if lu in diccionario_notas:
                notas_lu = diccionario_notas[lu]
            notas_lu.append(float(nota))
            diccionario_notas[lu] = notas_lu
    diccionario_notas_promediadas = {lu: promedio_lista_floats(diccionario_notas[lu]) for lu in diccionario_notas}
    with open(nombre_archivo_promedios, 'w') as f:
        lineas = []
        for lu in diccionario_notas_promediadas:
            lineas.append(f"{lu},{diccionario_notas_promediadas[lu]}\n")
        f.writelines(lineas)

calcular_promedio_por_estudiante("promedios_in.txt", "promedios_out.txt")

# 8 - pilas
from queue import LifoQueue as Stack
import random

def generar_nros_al_azar(cantidad: int, desde: int, hasta: int) -> Stack[int]:
    p: Stack[int] = Stack()
    for _ in range(0, cantidad):
        p.put(random.randint(desde, hasta))
    return p

nros = generar_nros_al_azar(3, 5, 10)
while not nros.empty():
    print(nros.get())

# 9
def cantidad_elementos(p: Stack) -> int:
    elementos_pila = []
    while not p.empty():
        elementos_pila.insert(0, p.get())
    for elemento in elementos_pila: # restauro p
        p.put(elemento)
    return len(elementos_pila)

nros = generar_nros_al_azar(15, 5, 10)
print(cantidad_elementos(nros)) # 3
print(cantidad_elementos(nros)) # 3 -> no modificada

# 10
def buscar_el_maximo(p: Stack[int]) -> int:
    maximo: int | None = None
    elementos_pila = []
    while not p.empty():
        elemento: int = p.get()
        if maximo == None:
            maximo = 0
        if elemento > maximo:
            maximo = elemento
        elementos_pila.insert(0, elemento)
    for elemento in elementos_pila: # restauro p
        p.put(elemento)
    return maximo

print(buscar_el_maximo(nros))

# 11
def esta_bien_balanceada(s: str) -> bool:
    pila_parentesis = Stack()
    for char in s:
        if char == '(':
            pila_parentesis.put(())
        elif char == ')':
            if pila_parentesis.empty():
                return False
            pila_parentesis.get()
    return pila_parentesis.empty()

print(esta_bien_balanceada("1 + ( 2 x 3 - ( 2 0 / 5 ))"))
print(esta_bien_balanceada("10 * ( 1 + ( 2 * ( -1 ) ))"))
print(esta_bien_balanceada("1 + ) 2 x 3 ( ()"))

# 12
def evaluar_expresion(s: str) -> float:
    elementos: Stack[float] = Stack()
    for v in s.split(" "):
        match v:
            case '+':
                elementos.put(elementos.get() + elementos.get())
            case '-':
                n1 = elementos.get()
                elementos.put(elementos.get() - n1)
            case '*':
                elementos.put(elementos.get() * elementos.get())
            case '/':
                elementos.put(elementos.get() / elementos.get())
            case operando:
                elementos.put(float(operando))
    return elementos.get()

print(evaluar_expresion("3 4 + 5 * 2 -"))

# 13 - colas
from queue import Queue
def generar_nros_al_azar_cola(cantidad: int, desde: int, hasta: int) -> Queue[int]:
    p: Queue[int] = Queue()
    for _ in range(0, cantidad):
        p.put(random.randint(desde, hasta))
    return p

nros = generar_nros_al_azar_cola(3, 5, 10)
while not nros.empty():
    print(nros.get())

# 14
def cantidad_elementos(c: Queue) -> int:
    copia = []
    total = 0
    while not c.empty():
        total += 1
        copia.append(c.get())
    for v in copia:
        c.put(v)
    return total

# 15
def buscar_el_maximo(c: Queue[int]) -> int:
    copia = []
    maximo = 0
    while not c.empty():
        el: int = c.get()
        if el > maximo:
            maximo = el
        copia.append(el)
    for v in copia:
        c.put(v)
    return maximo

# 16
def armar_secuencia_de_bingo() -> Queue[int]:
    numeros: list[int] = []
    for _ in range(0, 100):
        while True:
            rand = random.randint(0, 99)
            if numeros.count(rand) == 0:
                numeros.append(rand)
                break
    cola: Queue[int] = Queue()
    for v in numeros:
        cola.put(v)
    return cola

def armar_carton_de_bingo() -> list[int]:
    numeros: list[int] = []
    for _ in range(0, 12):
        while True:
            rand = random.randint(0, 99)
            if numeros.count(rand) == 0:
                numeros.append(rand)
                break
    return numeros

bolillero = armar_secuencia_de_bingo()
carton = armar_carton_de_bingo()

def jugar_carton_de_bingo(carton: list[int], bolillero: Queue[int]) -> int:
    jugadas = 0
    iteraciones_ok = 0
    while iteraciones_ok < len(carton):
        jugadas += 1
        bola = bolillero.get()
        if carton.count(bola) > 0:
            print("Ok:", bola)
            iteraciones_ok += 1
    return jugadas

print("Jugadas:", jugar_carton_de_bingo(carton, bolillero))

# 17
def n_pacientes_urgentes(c: Queue[(int, str, str)]) -> int:
    copia = []
    total = 0
    while not c.empty():
        v = c.get()
        total += 1 * (v[0] >= 1 and v[0] <= 3)
        copia.append(v)
    for v in copia:
        c.put(v)
    return total

colacolacola = Queue()
colacolacola.put((1, '', ''))
colacolacola.put((2, '', ''))
colacolacola.put((3, '', ''))
colacolacola.put((4, '', ''))
colacolacola.put((5, '', ''))
print(n_pacientes_urgentes(colacolacola))

# 18
"""
problema atencion_a_clientes(in c: Cola[(str, int, bool, bool)]): Cola[(str, int, boo, bool)] {
    requiere: {True}
    asegura: {|res| = |c|}
    asegura: {el pertenece a res <=> el pertenece a c}
    asegura: {el no pertenece a res <=> el no pertenece a c}
    asegura: {res es c reordenado en subgrupos por prioridad y cada subgrupo se ordena respetando el orden en res respectivo entre cada elemento}
}
"""

def atencion_a_clientes(c: Queue[(str, int, bool, bool)]) -> Queue[(str, int, bool, bool)]:
    cola_normal = Queue()
    cola_preferencial = Queue()
    cola_prioritaria = Queue()
    copia: list[(str, int, bool, bool)] = []
    while not c.empty():
        el = c.get()
        copia.append(el)
        if el[3]:
            cola_prioritaria.put(el)
        elif el[2]:
            cola_preferencial.put(el)
        else:
            cola_normal.put(el)
    res = Queue()
    while not cola_prioritaria.empty():
        res.put(cola_prioritaria.get())
    while not cola_preferencial.empty():
        res.put(cola_preferencial.get())
    while not cola_normal.empty():
        res.put(cola_normal.get())
    for v in copia:
        c.put(v)
    return res

# 19 - diccionarios
def agrupar_por_longitud(nombre_archivo: str) -> dict[int, int]:
    res: dict[int, int] = {}
    with open(nombre_archivo, 'r') as f:
        for linea in f.readlines():
            linea = linea.replace("\n", "")
            palabras = linea.split(" ")
            for palabra in palabras:
                long = len(palabra)
                if long == 0:
                    continue
                if long in res:
                    res[long] = res[long] + 1
                else:
                    res[long] = 1
    return res

print(agrupar_por_longitud("test.txt"))

# 20 jajaja ya lo había hecho con dicts
def calcular_promedio_por_estudiante_dict(nombre_archivo_notas: str):
    diccionario_notas: dict[str, list[float]] = {}
    with open(nombre_archivo_notas, 'r') as f:
        for linea in f.readlines():
            [lu, _, _, nota] = linea.split(',')
            notas_lu = []
            if lu in diccionario_notas:
                notas_lu = diccionario_notas[lu]
            notas_lu.append(float(nota))
            diccionario_notas[lu] = notas_lu
    diccionario_notas_promediadas = {lu: promedio_lista_floats(diccionario_notas[lu]) for lu in diccionario_notas}
    return diccionario_notas_promediadas

print(calcular_promedio_por_estudiante_dict("promedios_in.txt"))

# 21
def la_palabra_mas_frecuente(nombre_archivo: str) -> str:
    res: dict[str, int] = {}
    with open(nombre_archivo, 'r') as f:
        for linea in f.readlines():
            linea = linea.replace("\n", "")
            palabras = linea.split(" ")
            for palabra in palabras:
                if len(palabra) == 0:
                    continue
                if palabra in res:
                    res[palabra] = res[palabra] + 1
                else:
                    res[palabra] = 1
    maximo = ("", 0)
    for palabra in res:
        if res[palabra] > maximo[1]:
            maximo = (palabra, res[palabra])
    return maximo[0]

print(la_palabra_mas_frecuente("test.txt"))

# 22
def visitar_sitio(historiales: dict[str, Stack[str]], usuario: str, sitio: str):
    if usuario in historiales:
        historiales[usuario].put(sitio)
    else:
        pila = Stack()
        pila.put(sitio)
        historiales[usuario] = pila

def navegar_atras(historiales: dict[str, Stack[str]], usuario: str):
    actual = historiales[usuario].get()
    anterior = historiales[usuario].get()
    historiales[usuario].put(actual)
    historiales[usuario].put(anterior)

historiales = {}
visitar_sitio(historiales, "usuario1", "google.com")
visitar_sitio(historiales, "usuario1", "facebook.com")
navegar_atras(historiales, "usuario1")
visitar_sitio(historiales, "usuario2", "youtube.com")
# print(historiales)

# 23
# inventario: dict[str, dict[str, any]]
def agregar_producto(inventario: dict[str, dict[str]], nombre: str, precio: float, cantidad: int):
    inventario[nombre] = {
        "precio": precio,
        "cantidad": cantidad
    };

def actualizar_stock(inventario: dict[str, dict[str]], nombre: str, cantidad: int):
    inventario[nombre]["cantidad"] = cantidad

def actualizar_precios(inventario: dict[str, dict[str]], nombre: str, precio: float):
    inventario[nombre]["precio"] = precio

def calcular_valor_inventario(inventario: dict[str, dict[str]]) -> float:
    v = 0.0
    for key in inventario:
        if inventario[key]["cantidad"] > 0:
            v += inventario[key]["cantidad"] * inventario[key]["precio"]
    
    return v

inventario = {}
agregar_producto(inventario, "Camisa", 20.0, 50)
agregar_producto(inventario, "Pantalón", 30.0, 30)
actualizar_stock(inventario, "Camisa", 20)
valor_total = calcular_valor_inventario(inventario)
print("valor total:", valor_total)